﻿using UnityEngine;
using System.Collections.Generic;

namespace MainScripts
{
	public class Gfx : MonoBehaviour
	{
		Main main;
		public int myRes;

		public int screenWidth;
		public int screenHeight;
		public Vector3 myResVector;
		public Vector3 resetVector;
		public Camera cam;

		public int levelWidth;
		public int levelHeight;

		public GameObject level;
		private Vector3 levelPos;

		public GameObject background;

		Dictionary<string, Sprite[]> levelSprites;

		public void Init(Main inMain)
		{

			main = inMain;

			// --------------------------------------------------------------------------------
			// Setup Screen Resolution
			// --------------------------------------------------------------------------------

			screenWidth = main.screenWidth;
			screenHeight = main.screenHeight;
			resetVector = new Vector3(1.0f, 1.0f, 1.0f);
			myRes = Mathf.CeilToInt(screenHeight / 267F);

			int tMyRes = Mathf.CeilToInt(screenWidth / 440F);
			if (tMyRes > myRes)
			{
				myRes = tMyRes;
			}

			myResVector = new Vector3(myRes, myRes, 1.0f);

			// --------------------------------------------------------------------------------
			// Setup Camera
			// --------------------------------------------------------------------------------

			cam = main.cam;
			cam.pixelRect = new Rect(0, 0, screenWidth, screenHeight);
			cam.orthographicSize = screenHeight / 2;
			cam.transform.position = new Vector3(screenWidth / 2, -screenHeight / 2, -10f);

			// --------------------------------------------------------------------------------
			// Setup Level
			// --------------------------------------------------------------------------------

			level = new GameObject("Level");
			level.transform.localScale = myResVector;
			level.transform.position = new Vector3(0, 0, 1);
			levelPos = level.transform.position;

			levelSprites = new Dictionary<string, Sprite[]>();

			var inLevel = MakeGameObject("Level1", Resources.Load<Sprite>("Level/Level"), 0, 0, "Level");
			AddColliders(inLevel, "Level/Level_Floors", "LevelFloors");
			AddColliders(inLevel, "Level/Level_Walls", "LevelWalls");
			AddColliders(inLevel, "Level/Level_HideyHoles", "LevelHidey");

			background = MakeGameObject("Background", Resources.Load<Sprite>("Level/Background"),
				screenWidth / 2 / myRes, screenHeight / 2 / myRes, "Background");
			SetParent(background, null);

			SetPos(level, -684, -1416);
		}

		private GameObject AddColliders(GameObject inLevel, string spritePath, string layerName)
		{
			GameObject o = new GameObject();
			o.name = inLevel.name + "_" + layerName;
			o.layer = LayerMask.NameToLayer(layerName);
			SpriteRenderer sr = o.AddComponent<SpriteRenderer>();
			sr.sprite = Resources.Load<Sprite>(spritePath);
			o.transform.parent = inLevel.transform;
			o.transform.localScale = resetVector;
			Vector3 tPos = o.transform.localPosition;
			tPos.x = inLevel.transform.position.x;
			tPos.y = inLevel.transform.position.y;
			o.transform.localPosition = tPos;
			o.AddComponent<PolygonCollider2D>();
			sr.enabled = false;

			return o;
		}

		public Sprite[] GetLevelSprites(string inName)
		{

			if (!levelSprites.ContainsKey(inName))
			{
				levelSprites.Add(inName, Resources.LoadAll<Sprite>(inName));
			}

			return levelSprites[inName];
		}

		public void MoveLevel(float inX, float inY, float maxDelta)
		{
			float smoothX = Mathf.MoveTowards(level.transform.localPosition.x, -inX * myRes, maxDelta);
			SetPos(level, smoothX, -inY * myRes);
		}

		public GameObject MakeGameObject(string inName, Sprite inSprite, float inX = 0, float inY = 0,
			string inLayerName = "GameObjects")
		{

			GameObject o = new GameObject(inName);
			SpriteRenderer sr = o.AddComponent<SpriteRenderer>();
			sr.sprite = inSprite;
			o.transform.parent = level.transform;
			sr.sortingLayerName = inLayerName;
			o.transform.localScale = resetVector;
			Vector3 tPos = o.transform.localPosition;
			tPos.x = inX;
			tPos.y = -inY;
			o.transform.localPosition = tPos;

			return o;
		}

		public void SetParent(GameObject g, Transform inParent)
		{

			g.transform.parent = inParent;
		}

		public void SetScale(GameObject g, float inX, float inY)
		{

			Vector3 v = g.transform.localScale;
			v.x = inX;
			v.y = inY;
			g.transform.localScale = v;
		}

		public void SetScaleX(GameObject g, float inX)
		{

			Vector3 v = g.transform.localScale;
			v.x = inX;
			g.transform.localScale = v;

		}

		public void SetPos(GameObject g, float inX, float inY)
		{

			Vector3 v = g.transform.localPosition;
			v.x = inX;
			v.y = -inY;
			g.transform.localPosition = v;
		}

		public void SetPosX(GameObject g, float inX)
		{

			Vector3 v = g.transform.localPosition;
			v.x = inX;
			g.transform.localPosition = v;
		}

		public void SetPosY(GameObject g, float inY)
		{

			Vector3 v = g.transform.localPosition;
			v.y = -inY;
			g.transform.localPosition = v;
		}

		public void SetDirX(GameObject g, int inDir)
		{

			Vector3 v = g.transform.localScale;
			v.x = Mathf.Abs(v.x) * inDir;
			g.transform.localScale = v;
		}

		public void SetSprite(GameObject g, Sprite s)
		{

			g.GetComponent<SpriteRenderer>().sprite = s;
		}

		public float GetSpriteWidth(GameObject g)
		{
			return g.GetComponent<SpriteRenderer>().sprite.rect.width;
		}

		public float GetSpriteHeight(GameObject g)
		{
			return g.GetComponent<SpriteRenderer>().sprite.rect.height;
		}
	}
}