using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using GameScripts;

namespace MainScripts
{
    public class Game : MonoBehaviour
    {

        Main main;
        int myRes;
        Gfx gfx;
        Snd snd;

        static string PLAY = "play";
        private static string QUIT = "quit";
        string gameStatus;

        int camWidth;
        int camHeight;
        float camX;
        float camY;
        float camBias = 1;

        Player player;

        private bool leftKey, rightKey, jumpKey, duckKey, shootKey, specialKey;
        int playerHorizontal, playerVertical;
        bool playerShoot;
        bool playerShootRelease = true;
        private bool playerSpecial;
        private bool playerSpecialRelease = true;

        List<GeneralObject> gameObjects;
        int gameObjectLength;


        public void Init(Main inMain)
        {

            main = inMain;
            gfx = main.gfx;
            myRes = gfx.myRes;
            snd = main.snd;

            camWidth = gfx.screenWidth / myRes;
            camHeight = gfx.screenHeight / myRes;

            gameObjects = new List<GeneralObject>();
            gameObjectLength = 0;

            player = new Player(main, 370, 624);
            AddLevelObject(player);

            AddLevelObject(new Enemy(main, 530, 560));
            AddLevelObject(new Enemy(main, 516, 624));


            var randomTrack = Random.Range(0, snd.MusicTracks.Count);
            snd.PlayMusic(randomTrack);

            camX = player.localPosition.x - 5 * camWidth / 12;
            camY = 600 - camHeight / 2;
            gfx.MoveLevel(camX, camY, 5000);


            gameStatus = PLAY;

        }

        void Update()
        {

            if (gameStatus == PLAY)
            {

                GoKeys();

                GoPlayer();

                GoCam();

                GoObjects();
            }
        }

        void GoPlayer()
        {

            player.PlayerFrameEvent(playerHorizontal, playerVertical, playerShoot, playerSpecial);
        }

        private void GoKeys()
        {

            // ---------------------------------------------------------------
            // NORMAL KEYBOARD
            // ---------------------------------------------------------------

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                leftKey = true;
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                leftKey = false;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                rightKey = true;
            }

            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                rightKey = false;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                jumpKey = true;
            }

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                jumpKey = false;
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                duckKey = true;
            }

            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                duckKey = false;
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                specialKey = true;
            }

            if (Input.GetKeyUp(KeyCode.M))
            {
                specialKey = false;
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                shootKey = true;
            }

            if (Input.GetKeyUp(KeyCode.N))
            {
                shootKey = false;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            playerHorizontal = 0;
            if (leftKey)
            {
                playerHorizontal -= 1;
            }

            if (rightKey)
            {
                playerHorizontal += 1;
            }

            playerVertical = 0;
            if (jumpKey)
            {
                playerVertical -= 1;
            }

            if (duckKey)
            {
                playerVertical += 1;
            }

            playerSpecial = false;
            if (specialKey)
            {
                if (playerSpecialRelease)
                {
                    playerSpecialRelease = false;
                    playerSpecial = true;
                }
            }
            else
            {
                if (!playerSpecialRelease)
                {
                    playerSpecialRelease = true;
                }
            }

            playerShoot = false;
            if (shootKey)
            {
                if (playerShootRelease)
                {
                    playerShootRelease = false;
                    playerShoot = true;
                }

            }
            else
            {
                if (!playerShootRelease)
                {
                    playerShootRelease = true;
                }
            }
        }

        void GoCam()
        {
            switch (playerHorizontal)
            {
                case 0:
                    break;
                case var exp when playerHorizontal < 0:
                    camBias = 7;
                    break;
                case var exp when playerHorizontal > 0:
                    camBias = 5;
                    break;
            }

            camX = player.localPosition.x - camBias * camWidth / 12;
            camY = 600 - camHeight / 2;

            float maxDelta = Math.Abs(player.xDelta) * 5f;

            if (playerHorizontal != 0)
                gfx.MoveLevel(camX, camY, maxDelta);
        }

        public void AddLevelObject(GeneralObject inObj)
        {

            gameObjects.Add(inObj);
            gameObjectLength++;
        }

        void GoObjects(bool inDoActive = true)
        {

            for (int i = 0; i < gameObjectLength; i++)
            {

                if (!gameObjects[i].FrameEvent())
                {
                    gameObjects[i].Destroy();
                    gameObjects.RemoveAt(i);
                    i--;
                    gameObjectLength--;
                }
            }
        }
    }
}