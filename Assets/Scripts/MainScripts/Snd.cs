using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace MainScripts
{
    public class Snd : MonoBehaviour
    {
        public Dictionary<string, AudioClip> AudioClips;
        public Dictionary<string, AudioClip> MusicTracks;

        Main main;
        GameObject snd;
        GameObject msc;
        AudioSource audioSource;
        AudioSource musicSource;


        public void Init(Main inMain)
        {

            main = inMain;
            snd = new GameObject("Sound");
            audioSource = snd.AddComponent<AudioSource>();

            msc = new GameObject("Music");
            musicSource = msc.AddComponent<AudioSource>();

            AudioClips = new Dictionary<string, AudioClip>();
            MusicTracks = new Dictionary<string, AudioClip>();

            AddAudioClip("Gun", "Audio/Gun");
            AddMusicClip("track01", "Audio/Gigakoops_Shark");
            AddMusicClip("track02", "Audio/Gigakoops_Tunnel");

        }

        public void PlayAudioClip(string inVer, float volume = 1)
        {
            audioSource.PlayOneShot(AudioClips[inVer], volume);
        }

        public void AddAudioClip(string inId, string inAddress)
        {
            AudioClip tClip = Resources.Load<AudioClip>(inAddress);
            AudioClips.Add(inId, tClip);
            tClip.LoadAudioData();
        }

        public void AddMusicClip(string inId, string inAddress)
        {
            AudioClip tClip = Resources.Load<AudioClip>(inAddress);
            MusicTracks.Add(inId, tClip);
            tClip.LoadAudioData();
        }


        public void PlayMusic(int track, float volume = 1, bool loop = true)
        {
            musicSource.loop = loop;
            var clip = MusicTracks.ElementAt(track).Value;
            if (clip)
                musicSource.PlayOneShot(clip, volume);
        }
    }
}
