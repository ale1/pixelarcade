using UnityEngine;
using MainScripts;

namespace GameScripts
{
	public class GeneralObject
	{
		protected float x;
		protected float y;
		protected bool isOK = true;
		protected Main main;
		protected Game game;
		protected Gfx gfx;
		protected Snd snd;
		protected Sprite[] sprites;
		protected int direction;
		protected GameObject gameObject;

		protected void SetGeneralVars(Main inMain, int inX, int inY)
		{
			main = inMain;
			game = main.game;
			gfx = main.gfx;
			snd = main.snd;
			x = inX;
			y = inY;
		}

		public virtual bool FrameEvent()
		{
			return false;
		}

		public virtual void Kill()
		{
			isOK = false;
		}

		public void Destroy()
		{
			if (gameObject != null)
				UnityEngine.Object.Destroy(gameObject);
		}
	}
}