﻿
namespace GameScripts.PlayerStates
{
    public abstract class PlayerState
    {
        public static Player player;

        public static int AnimRate = 4;
        
        public int Channel { get; protected set; }
        
        public PlayerState()
        {
            
        }

        //Base StateMachine behaviours
        public abstract void OnFrameEvent(int frameCount);
        public abstract void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial);
        public abstract void OnStateEnter();

    }
}