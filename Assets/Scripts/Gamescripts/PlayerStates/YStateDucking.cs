﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class YStateDucking : PlayerState
    {
        private bool _cancel;
        private bool _diving;

        public YStateDucking()
        {
            this.Channel = player.FindChannel<YStateDucking>();
        }

        public override void OnStateEnter()
        {
            player.speed = 0;
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            player.speed = 0;
            player.gravity = 0;

            if (inMoveY != 1)
            {
                _cancel = true;
            }
            else if (inSpecial)
            {
                var floorHit = Physics2D.Raycast(
                    player.worldPosition + new Vector3(0, -500f, 0),
                    Vector2.up,
                    510f,
                    LayerMask.GetMask("LevelFloors")
                );

                if (floorHit.distance < 400)
                {
                    _diving = true;
                }
            }
        }

        public override void OnFrameEvent(int frameCount)
        {
            if (_cancel)
            {
                player.SetState<YStateGrounded>();
            }
            else if (_diving)
            {
                player.SetState<YStateDiving>();
            }
        }
    }
}