﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class ActionStateHiding : PlayerState
    {
        private bool cancel;
        
        public ActionStateHiding()
        {
            this.Channel = 2;
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            player.speed = 0;
            player.gravity = 0;
            
            if (inSpecial)
                cancel = true;
        }

        public override void OnStateEnter()
        {
            player.speed = 0;
            player.gravity = 0;
        }

        public override void OnFrameEvent(int frameCount)
        {
            if (cancel)
            {
                player.SetState<ActionStateNone>();
            }
        }

    }
}


