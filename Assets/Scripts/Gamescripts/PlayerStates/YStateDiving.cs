﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class YStateDiving : PlayerState
    {
        private int timer = 12;
        public YStateDiving()
        {
            this.Channel = player.FindChannel<YStateDiving>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {

        }

        public override void OnStateEnter()
        {
   
        }

        public override void OnFrameEvent(int frameCount)
        {
            timer -= 1;
            player.gravity += 0.15f;

            var floorHits = Physics2D.RaycastAll(
                player.worldPosition,
                Vector2.down,
                100f,
                LayerMask.GetMask("LevelFloors")
            );
            
            if (timer <= 0)
            {
                player.SetState<YStateAir>();
            }
        }
    }
}