﻿using UnityEngine;
using System;

namespace GameScripts.PlayerStates
{
    public class XStateMoving : PlayerState
    {
        private int _inMoveX;
        
        public XStateMoving()
        {
            this.Channel = player.FindChannel<XStateMoving>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            _inMoveX = inMoveX;
            if (inMoveX == 0)
                player.speed = 0;
            else if (inMoveX != _inMoveX) // more realistic change of direction during jump
                player.speed = 0.04f;
        }

        public override void OnStateEnter()
        {
            player.speed = 0.01f;
        }
        
        public override void OnFrameEvent(int frameCount)
        {
            var wallHit = Physics2D.Raycast(
                player.worldPosition + new Vector3(_inMoveX, 0, 0),
                _inMoveX > 0 ? Vector2.right : Vector2.left,
                4f,
                LayerMask.GetMask("LevelWalls")
            );
            
            if (wallHit.collider == null  && player.speed != 0)
            {
                player.speed = Math.Max(0.1f, Math.Min(player.speed + 0.025f, 1.6f));
            }
            else
            {
                player.SetState<XStateIdle>();
            }
        }

    }
}


