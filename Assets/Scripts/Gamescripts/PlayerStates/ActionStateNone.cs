﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class ActionStateNone : PlayerState
    {
        private bool _inShoot;
        private bool _inSpecial;
        
        public ActionStateNone()
        {
            this.Channel = player.FindChannel<ActionStateNone>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            _inShoot = inShoot;
            _inSpecial = inSpecial;
        }

        public override void OnStateEnter()
        {
           
        }

        public override void OnFrameEvent(int frameCount)
        {
            if (_inShoot)
            {
                player.SetState<ActionStateShooting>();
            }
            else if (_inSpecial)
            {
                var coll = Physics2D.OverlapPoint(player.worldPosition, LayerMask.GetMask("LevelHidey"));
                var overlap = (coll != null);
                if(overlap) {player.SetState<ActionStateHiding>();}
            }
        }
    }
}