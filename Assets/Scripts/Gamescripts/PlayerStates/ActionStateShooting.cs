﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class ActionStateShooting : PlayerState
    {
        public ActionStateShooting()
        {
            this.Channel = player.FindChannel<ActionStateShooting>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
  
        }

        public override void OnStateEnter()
        {
            player.actionFrames = 6 * 4;
            player.SpawnBullet();

        }

        public override void OnFrameEvent(int frameCount)
        {
            player.actionFrames--;

            if (player.actionFrames <= 0) //has finished shooting, go back to idle
            {
                player.SetState<ActionStateNone>();
            }
        }
    }
}


