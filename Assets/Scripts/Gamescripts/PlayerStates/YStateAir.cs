﻿
using System;
using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class YStateAir : PlayerState
    {
        public YStateAir() 
        {
            this.Channel = player.FindChannel<YStateAir>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            
        }

        public override void OnStateEnter()
        {

        }

        public override void OnFrameEvent(int frameCount)
        {
            player.gravity += 0.15f;
            
            var floorHit = Physics2D.Raycast(
                player.worldPosition,
                Vector2.down,
                120f,
                LayerMask.GetMask("LevelFloors")
            );

            if (floorHit.collider == null) //falling
            {
               
            }
            else if( floorHit.distance > player.gravity) //falling
            {
                player.gravity = player.gravity > floorHit.distance ? floorHit.distance : player.gravity;
            }
            else
            {
                //has touched ground
                //player.SetState(new YStateGrounded(player));
                player.SetState<YStateGrounded>();
            }
        }
    }
}
