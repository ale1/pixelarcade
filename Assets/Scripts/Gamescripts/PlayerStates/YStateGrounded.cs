﻿

using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class YStateGrounded : PlayerState
    {
        private bool ducking = false;
        
        public YStateGrounded()
        {
            this.Channel = player.FindChannel<YStateGrounded>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            if (inMoveY == -1)
            {
                player.gravity -= 5f;
            }
            else if (inMoveY == 1)
            {
                ducking = true;
            }
        }

        public override void OnStateEnter()
        {
            player.gravity = 0;
        }

        public override void OnFrameEvent(int frameCount)
        {
            var floorHit = Physics2D.Raycast(
                player.worldPosition,
                Vector2.down,
                50f,
                LayerMask.GetMask("LevelFloors")
            );

            if (floorHit.collider == null || floorHit.distance > 1 + player.gravity)
            {
                player.SetState<YStateAir>();
            }
            else if(ducking)
            {
                player.SetState<YStateDucking>();
            }
        }
    }
}



