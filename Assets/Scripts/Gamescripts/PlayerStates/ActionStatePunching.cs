﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class ActionStatePunching : PlayerState
    {
        public ActionStatePunching()
        {
            this.Channel = player.FindChannel<ActionStatePunching>();
            Debug.Log(nameof(ActionStatePunching) + "--" + this.Channel.ToString());
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            //todo
        }

        public override void OnStateEnter()
        {

        }

        public override void OnFrameEvent(int frameCount)
        {
            //todo
        }
    }
}



