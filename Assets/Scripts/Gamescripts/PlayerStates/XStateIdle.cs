﻿
using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class XStateIdle : PlayerState
    {
        private int _inMoveX;
        
        public XStateIdle()
        {
            this.Channel = 0;
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            _inMoveX = inMoveX;
        }

        public override void OnStateEnter()
        {
            player.speed = 0;
        }

        public override void OnFrameEvent(int frameCount)
        {
            var wallHit = Physics2D.Raycast(
                player.worldPosition + new Vector3(_inMoveX, 0, 0),
                _inMoveX > 0 ? Vector2.right : Vector2.left,
                4f,
                LayerMask.GetMask("LevelWalls")
            );
            
            if (wallHit.collider == null  && _inMoveX != 0)
            {
                player.SetState<XStateMoving>();
            }
        }
    }
}



