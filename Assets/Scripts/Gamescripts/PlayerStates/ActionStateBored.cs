﻿using UnityEngine;

namespace GameScripts.PlayerStates
{
    public class ActionStateBored : PlayerState
    {
        private bool _inShoot;
        private bool _inSpecial;
        
        public ActionStateBored()
        {
            this.Channel = player.FindChannel<ActionStateBored>();
        }

        public override void OnInputEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            _inShoot = inShoot;
            _inSpecial = inSpecial;
        }

        public override void OnStateEnter()
        {
             
        }

        public override void OnFrameEvent(int frameCount)
        {
            if (_inShoot)
            {
                player.SetState<ActionStateShooting>();
            }
            else if(_inSpecial)
            {
                var overlap = Physics2D.OverlapPoint(player.localPosition, LayerMask.GetMask("LevelHidey"));
                Debug.LogError("testing for overlap!");
                Debug.LogError(overlap);
                if(overlap) {player.SetState<ActionStateHiding>();}    
          
            }
        }
    }
}