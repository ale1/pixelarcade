﻿using UnityEngine;

namespace GameScripts
{
    public class Bullet : GeneralObject
    {

        private float _speed = 4.8f;
        private int duration = 60 * 3;

        public Bullet(Main inMain, int inX, int inY, int direction)
        {
            SetGeneralVars(inMain, inX, inY);
            sprites = gfx.GetLevelSprites("Players/Bullet");

            gameObject = gfx.MakeGameObject("Bullet", sprites[0], x, y);

            SetDirection(direction > 0 ? 1 : -1);
            _speed *= direction;
            
            snd.PlayAudioClip("Gun");
        }

        public override bool FrameEvent()
        {
            duration--;
            if (duration <= 0)
                Kill();

            var mask = LayerMask.GetMask("Default", "LevelWalls", "LevelFloors");
            var coll = Physics2D.OverlapPoint(gameObject.transform.position, mask);
            var overlap = (coll != null);


            if (overlap)
            {
                var collLayer = coll.gameObject.layer;
                {
                    if ((int) LayerMask.NameToLayer("LevelWalls") == collLayer)
                    {
                        Kill();
                    }
                    else if ((int) LayerMask.NameToLayer("LevelFloors") == collLayer)
                    {
                        Kill();
                    }
                    else if ((int) LayerMask.NameToLayer("Default") == collLayer)
                    {
                        Kill();
                    }
                }
            }


            x = x + _speed;
            UpdatePos();

            return isOK;
        }


        void UpdatePos()
        {
            gfx.SetPos(gameObject, x, y);
        }

        void SetDirection(int inDirection)
        {
            direction = inDirection;
            gfx.SetDirX(gameObject, direction);
        }
    }
}