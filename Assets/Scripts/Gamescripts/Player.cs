﻿using System;
using UnityEngine;
using System.Linq;
using GameScripts.PlayerStates;

namespace GameScripts
{
    public class Player : GeneralObject
    {
        private int _frameCount = 0;

        private int _spriteIndex = 0;
        private int[] SpritesRun = {0, 1, 2, 3, 4, 5};
        private int[] SpritesShoot = {21,14, 15, 16, 17, 21};
        private int[] SpritesIdle = {22, 23, 24, 25};
        private int[] SpritesJump = {26};
        private int[] SpritesAir = {27};
        private int[] SpritesDuck = {28};
        private int[] SpritesHide = {29};
        private int[] SpritesRunShoot = {30, 31, 32, 33, 34, 35};



        public Vector3 worldPosition => gameObject.transform.position;
        public Vector3 localPosition => gameObject.transform.localPosition;

        public Vector3 GetDirection => direction < 0 ? Vector3.right : Vector3.left;

        private Collider2D collider;
        

        public float xDelta { get; private set; }
        
        public float gravity = 0;
        public float speed = 0;
        public int actionFrames = 0;

        
        //player can hold 3 states at once. But only 1 per "channel" of player control. [x,y,action];
        private PlayerState[] _currentStates;
        
        //states allowed for player, and what channel they belong in.
        private readonly Type[][] StateMatrix =
        {
            new Type[] { typeof(XStateIdle),        typeof(XStateMoving)},
            new Type[] { typeof(YStateGrounded),    typeof(YStateDucking),        typeof(YStateAir),         typeof(YStateDiving)},
            new Type[] { typeof(ActionStateNone),   typeof(ActionStateShooting),  typeof(PlayerStateDying),  typeof(ActionStateHiding), typeof(ActionStatePunching)}
        };
        
        public Player(Main inMain, int inX, int inY)
        {
            SetGeneralVars(inMain, inX, inY);
            
            game = main.game;
            gfx = main.gfx;
            snd = main.snd;
            PlayerState.player = this;
            
            direction = 1;
            
            sprites = gfx.GetLevelSprites("Players/Player1");
            gameObject = gfx.MakeGameObject("Player", sprites[SpritesIdle[0]], x, y, "Player");

            _currentStates = new PlayerState[] {new XStateIdle(), new YStateGrounded(), new ActionStateNone()};
        }
        
        public int FindChannel<T>() where T : PlayerState
        {
            for(int i = 0; i < StateMatrix.Length; i++)
            {
                if (StateMatrix[i].Contains(typeof(T))) { return i; }
            }
            return -1;
        }
        
        public void SpawnBullet()
        {
            int xPos = (int) (x + gfx.GetSpriteWidth(gameObject) * 0.5 * direction);
            int yPos = (int) (y + gfx.GetSpriteHeight(gameObject) * -0.70);
            game.AddLevelObject(new Bullet(main,xPos,yPos, direction));
        }

        public void SetState<T>() where T : PlayerState, new()
        {
            var newState = new T();

            _frameCount = 0;
            if(newState.Channel < 0)
                Debug.LogError("forgot to set channel:" + newState);
            
            int ch = newState.Channel;
            var state = _currentStates[ch];
            if (state != null && state.GetType() == newState.GetType())
            {
                Debug.LogError($"trying to enter state when already in it: {state.GetType() }{newState.GetType()}");
                return;
            }

            state = newState;
            gameObject.name = String.Concat("Player: ", (string) state.GetType().Name);
            _currentStates[ch] = state;
            _currentStates[ch].OnStateEnter();
        }

        private bool Is<T>() => _currentStates.OfType<T>().Any();
        
        
        public void PlayerFrameEvent(int inMoveX, int inMoveY, bool inShoot, bool inSpecial)
        {
            
            ++_frameCount;

            if (_frameCount > 3600)
                _frameCount = 0;
            
            for (int i = 0; i < _currentStates.Length; i++)
            {
                if (_currentStates[i] != null)
                {
                    _currentStates[i].OnInputEvent(inMoveX, inMoveY, inShoot, inSpecial);
                    _currentStates[i].OnFrameEvent(_frameCount);
                }
            }

            y += gravity;
            x += speed * inMoveX;
            
            UpdatePos();
            
            if (inMoveX > 0)
            {
                gfx.SetDirX(gameObject, inMoveX);
                direction = inMoveX;
            }
            else if (inMoveX < 0)
            {
                gfx.SetDirX(gameObject, inMoveX);
                direction = inMoveX;
            }
        }
        
        
        public override bool FrameEvent()
         {
            if (Is<YStateDucking>())
            {
                SetSpriteByNum(SpritesDuck[0]);
            }
            else if (Is<ActionStateHiding>())
            {
                SetSpriteByNum(SpritesHide[0]);
            }
            else if (Is<XStateMoving>() && Is<YStateGrounded>() && Is<ActionStateShooting>()) // run shoot
            {
                bool RunTick = _frameCount % Mathf.FloorToInt(Mathf.Min(16f, 8 / speed)) == 0; 
                if (RunTick)
                {
                    _spriteIndex = (_spriteIndex >= SpritesRunShoot.Length - 1) ? 0 : _spriteIndex + 1;
                    SetSpriteByNum(SpritesRunShoot[_spriteIndex]);
                }
            }
            else if (Is<XStateMoving>() && Is<YStateGrounded>() && Is<ActionStateNone>()) // running
            {
                bool RunTick = _frameCount % Mathf.FloorToInt(Mathf.Min(16f, 8 / speed)) == 0; 
                if (RunTick) 
                {
                    _spriteIndex = (_spriteIndex >= SpritesRun.Length - 1) ? 0 : _spriteIndex + 1;
                    SetSpriteByNum(SpritesRun[_spriteIndex]);
                }   
            }
            else if (Is<XStateIdle>() && Is<YStateGrounded>() && Is<ActionStateNone>()) //standing still
            {
                bool IdleTick = (_frameCount % 32 == 0); 
                if (IdleTick)
                {
                    _spriteIndex = (_spriteIndex >= SpritesIdle.Length - 1) ? 0 : _spriteIndex + 1;
                    SetSpriteByNum(SpritesIdle[_spriteIndex]);
                }
            }
            else if (Is<XStateIdle>() && Is<YStateGrounded>() && Is<ActionStateShooting>()) // shooting while standing
            {
                bool shootingTick = (_frameCount % 4 == 0); 
                    if (shootingTick)
                    {
                        _spriteIndex = (_spriteIndex >= SpritesShoot.Length - 1) ? 0 : _spriteIndex + 1;
                        SetSpriteByNum(SpritesShoot[_spriteIndex]);
                    }
            }
            else if (Is<YStateDiving>())
            {
                SetSpriteByNum(SpritesAir[0]);
            }
            else if (Is<YStateAir>() && Is<ActionStateShooting>())  // shooting while airborne
            {
                SetSpriteByNum(SpritesAir[0]);
            }
            else if (Is<YStateAir>() && Is<ActionStateNone>()) //airborne
            {
                SetSpriteByNum(SpritesAir[0]);
            }
            else
            {
                Debug.LogError($"unknown state combination:" +
                               _currentStates[0] 
                               + "--" +
                               _currentStates[1]
                               + "--" +
                               _currentStates[2]);
            }
            
            return isOK;
        }

        void UpdatePos()
        {
            xDelta = localPosition.x - x;
            gfx.SetPos(gameObject,x, y);
        }
        

        private void SetSpriteByNum(int num)
        {
            gfx.SetSprite(gameObject, sprites[num]);
        }
        
    }
}